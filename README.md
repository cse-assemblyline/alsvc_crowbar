# Crowbar Service

NOTE: This service does not require you to buy any licence and is preinstalled and working after a default installation.

Static script de-obfuscator. 
The purpose is not to get surgical de-obfuscation, but rather to extract useful indicators (uses FrankenStrings patterns.py). 

### Stage 1 Modules (in order of execution):
1. HTML javascript extraction
### Stage 2 Modules (in order of execution):
1. VBE Decode
2. Concat strings
3. MSWord macro vars
4. Powershell vars
5. String replace
6. Powershell carets
7. Array of strings
8. Fake array vars
9. Reverse strings
10. B64 Decode
11. Simple XOR function
12. Charcode
13. Charcode hex